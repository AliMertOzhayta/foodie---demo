# Foodie - Demo

Food ordering experience is improved by <b>Augmented Reality</b> for <b>iOS</b> devices.
<b>Django REST framework</b> is used as a web service and as database <b>PostgreSQL</b> is used.

![](demo/Foodie_App_Demo.mp4)


<a href="https://www.dropbox.com/s/vhqqb3xc2dxlsa5/Foodie%20App%20Demo.mp4?dl=0">If you can't view the video, please press here..</a>


# Source codes of the API and the application are in the different private repositories!!

